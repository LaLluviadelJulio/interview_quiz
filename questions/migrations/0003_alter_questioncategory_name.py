# Generated by Django 3.2.2 on 2024-01-10 10:43

from django.db import migrations, models
import questions.models


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0002_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questioncategory',
            name='name',
            field=models.CharField(max_length=64, unique=True),
        ),
    ]
