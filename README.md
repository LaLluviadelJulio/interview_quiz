<!-- PROJECT SHIELDS -->
[![MIT License][license-shield]][license-url]
<p><a name="readme-top"></a></p>


<!-- PROJECT LOGO -->
<br />
<div align="center">
  <h2 align="center">Interview quiz</h2>

  <p align="center">
    The simple website for theory training.
    <br />
    <a href="http://194.58.119.60/"><strong>To the Interview quiz website »</strong></a>

<br />
<a href="https://glcdn.githack.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/index.html">
<strong>To the docs »</strong></a>

  </p>
</div>


<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
      <ul>
        <li><a href="#launch">Launch</a></li>
        <li><a href="#auxiliary-commands">Auxiliary commands</a></li>
        <li><a href="#on-site">Main page and authorization</a></li>
        <li><a href="#testing">Testing process</a></li>
        <li><a href="#posts">Posts and post searching</a></li>
        <li><a href="#user-panel">Profile and user abilities</a></li>
        <li><a href="#custom-admin">Custom admin panel</a></li>
      </ul>
    <li><a href="#goals">Goals</a></li>
    <li><a href="#time-spent">Time spent</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>


<!-- ABOUT THE PROJECT -->
### About The Project
<p><a name="about-the-project"></a></p>

After registration, you can perform tests with questions of the required level of complexity to train different areas of knowledge.
The questions for each test are selected in a pseudo-random way.
In addition to questions, there are short posts on the site for a better understanding of the topic.
Any user can offer their question, post or write to the staff. 
For the convenience of moderating the site, there is its own admin panel.


<p align="right">(<a href="#readme-top">back to top</a>)</p>


### Built With
<p><a name="built-with"></a></p>

[![Python][Python logo]][Python url]
[![Django][Django logo]][Django url]
[![PostgreSQL][PostgreSQL logo]][PostgreSQL url]
[![JavaScript][JavaScript logo]][JavaScript url]
[![JQuery][JQuery logo]][JQuery url]
[![Bootstrap][Bootstrap logo]][Bootstrap url]
[![AJAX][AJAX logo]][AJAX url]
[![Kafka][Kafka logo]][Kafka url]
[![python-telegram-bot][python-telegram-bot logo]][python-telegram-bot url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- USAGE EXAMPLES -->
### Usage
<p><a name="usage"></a></p>

#### Launch
<p><a name="launch"></a></p>

The launch is possible both using docker and directly.

#### Auxiliary commands
<p><a name="auxiliary-commands"></a></p>

The project uses some commands available from the console for convenience.

<hr>

`python manage.py cr_su`

Allows you to quickly create a superuser.
<hr>

`python manage.py run_consumer`

Launching a consumer to start analyzing user progress.
<hr>

`python manage.py run_bot`

Launching Telegram bot for blitz testing
<hr>

`python manage.py set_webhook -u <selected url>`

Set a webhook if necessary, configuring tunneling to selected url.

`python manage.py set_webhook -u`

Leave the flag *-u* empty to delete the assigned webhook

`python manage.py set_webhook -u 9e9bfe2d18ff99.lhr.life 
/tlg/some_url`

Example of webhook assignment

<hr>

#### Main page and authorization
<p><a name="on-site"></a></p>

The main page of the site

[![Main](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/01_main_page.png)]()

You must log in to use all the functions of the site

[![Auth](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/02_auth.png)]()

Or you should register if you don't have an account. Use your email or VK social media account

[![Register](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/03_register.png)]()


#### Testing process
<p><a name="testing"></a></p>

First you have to select a category of questions to test

[![Categories](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/04_categories.png)]()

Then, at the start, you will be asked to select the level of the questions.
You can also enable/disable passing questions with a time limit.
If this option is active, 15 seconds will be given for 1 question.

[![Start](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/05_start.png)]()

Next, a question will be displayed on the screen where you can choose
the answer that seems right to you

[![Question](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/06_question.png)]()

Most of the questions are provided with an image that can be zoomed in

[![Question image](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/07_question_image.png)]()

If the time limit option is selected, and you do not give
an answer in the allotted time, testing will end

[![Time is up](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/08_time_is_up.png)]()

When you have selected an answer, you will immediately
see the result of your answer.
You can end the test now or complete it.
There are 20 pseudo-random questions in each test, or if there are fewer questions written for this topic and the difficulty level, then the test will contain all available questions.

[![Right answer](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/09_answer_right.png)]()

Your score will be increased by 1, 2 or 3 points
(depending on the difficulty level of the question)
if you answered correctly. If you make a mistake,
this score will be deducted from your total score
(unless, of course, it is equal to 0)

[![Wrong answer](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/10_wrong_answer.png)]()

You will also see your answer and the correct one
and will be able to compare them. Most of the questions
have pictures to explain the answer, which can also be 
zoomed. If there are 2 images, you can scroll through them
like a carousel and view.

There are posts (small articles) for some questions that
you can read in order to better remember the topic.

[![Carousel answer](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/11_right_answer_carousel.png)]()

At the end of the test, you will see how many correct
and erroneous answers you have given.

[![End](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/12_end.png)]()

#### Posts and post searching
<p><a name="posts"></a></p>

In the Posts tab of the site, you can read posts of the categories
you are interested in.

[![Posts](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/13_posts.png)]()

The post has a small format and is convenient for quick reading.

[![Post](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/14_post.png)]()

If you click on the author's nickname, you can view all the posts
he wrote and go to the one you need.

[![User posts](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/15_user_posts.png)]()

You can see the search bar at the top of the site.
It allows you to find a post by part of the title or tag.

[![Search posts](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/16_search.png)]()


#### Profile and user abilities
<p><a name="user-panel"></a></p>

The user can view his profile, edit his avatar,
first and last name (AJAX is used).
He will see his score and place in the ranking.

Additional buttons will appear in the menu.
They give you the opportunity to see the top 5 best players,
offer your question, post or write an arbitrary message
to the admin.

[![Profile](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/17_profile.png)]()

The top 5 will show information about the best players.
By clicking on their nickname, you can go to their posts.

[![Top](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/18_top.png)]()

If the user writes an message to the admin or offers new post
or question, the admin will immediately receive a notification
about this to his email.
The user can add illustrations to their posts/questions.
The post or question will be moderated, and if it is a good
contribution to the site's piggy bank, the admin will edit
and activate it.

[![New post](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/19_new_post.png)]()


#### Custom admin panel
<p><a name="custom-admin"></a></p>

The main page of the admin panel allows you to work with users,
categories of questions, questions and posts.

[![Admin main](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/20_admin_main.png)]()

All 4 varieties have similar functionality.
In this case, all users are displayed as a table.
You can edit, deactivate, completely delete and create new ones.
The page will not reload because AJAX technology is used.
You can also assign a user as a moderator to give him access to the admin panel.
The list of users (as well as questions, posts, categories) can be sorted by any attribute.

[![Admin users](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/21_admin_users.png)]()

The button for creating a new category, user, post, question is located at the bottom of the page.

[![Admin categories](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/22_admin_cat.png)]()

The pages for creating and editing a new entity are also similar for all 4 varieties.

[![Admin new question](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/23_admin_que.png)]()

Posts and questions can be filtered by category.

[![Posts category](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/24_posts_cat.png)]()

Tags are also used for these types of objects.
You can view all the tags that are currently involved.

[![Tags](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/25_tags.png)]()

Clicking on the tag will take you to the list of posts with this tag.
In addition, you can search in the admin panel by questions,
categories, posts, or users.

[![Tags port](https://gitlab.com/LaLluviadelJulio/interview_quiz/-/raw/master/documentation/images/26_tags_port.png)]()

### Goals
<p><a name="goals"></a></p>

* consolidation of knowledge and further study of Django;
* learning jQuery, AJAX, Javascript, Docker, Kafka, Telegram bot building etc.;
* a website for my own training of theoretical knowledge through passing tests;

### Time spent
<p><a name="time-spent"></a></p>
The development was carried out as a pet project in parallel with the study.
Individual additions were made subsequently. Improvements are possible in the future.

Time spent:
* basic logic - 1 month;
* documentation - 3 days;
* tests, optimization - 1 week;
* correction of identified problems - 1 week;
* kafka service for user progress analyze - 2 days;
* telegram bot - 1 week;

<!-- LICENSE -->
### License
<p><a name="license"></a></p>

Distributed under the MIT License.

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- CONTACT -->
### Contact
<p><a name="contact"></a></p>

Marina Marmalyukova - inspiracion@yandex.ru

Project Link: [https://gitlab.com/LaLluviadelJulio/csa-lite](https://gitlab.com/LaLluviadelJulio/csa-lite)

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[int-quiz]: http://194.58.119.60/
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[Python logo]: https://img.shields.io/badge/Python_3.8-3776AB?style=for-the-badge&logo=python&logoColor=white
[Python url]: https://www.python.org/?hl=RU
[Django logo]: https://img.shields.io/badge/Django_3.2-07405E?style=for-the-badge&logo=django&logoColor=white
[Django url]: https://www.djangoproject.com/
[PostgreSQL logo]: https://img.shields.io/badge/PostgreSQL-218440?style=for-the-badge&logo=postgresql&logoColor=white
[PostgreSQL url]: https://www.postgresql.org/
[JavaScript logo]: https://img.shields.io/badge/JavaScript-634890?style=for-the-badge&logo=javascript&logoColor=white
[JavaScript url]: https://www.javascript.com/
[JQuery logo]: https://img.shields.io/badge/JQuery-773452?style=for-the-badge&logo=jquery&logoColor=white
[JQuery url]: https://jquery.com/
[Bootstrap logo]: https://img.shields.io/badge/Bootstrap-dd2244?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap url]: https://getbootstrap.com/
[AJAX logo]: https://img.shields.io/badge/AJAX-c2341a?style=for-the-badge&logo=ajax&logoColor=white
[AJAX url]: https://curlie.org/Computers/Programming/Languages/JavaScript/AJAX
[Kafka logo]: https://img.shields.io/badge/Kafka-bbac34?style=for-the-badge&logo=apache-kafka&logoColor=white
[Kafka url]: https://kafka.apache.org/
[python-telegram-bot logo]: https://img.shields.io/badge/python_telegram_bot-123456?style=for-the-badge&logoColor=white
[python-telegram-bot url]: https://docs.python-telegram-bot.org/en/stable/index.html
