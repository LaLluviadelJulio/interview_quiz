"""Automatically created by Django to configure the web application."""
from django.apps import AppConfig


class UserLogConfig(AppConfig):
    """UserLog app configuration, automatically created Django class."""

    default_auto_field = 'django.db.models.BigAutoField'
    name = 'user_log'
