"""
User statistics package.

The **user_logging** package is necessary for processing user
statistics on the number of completed questions and the time
of the last login.
"""
