"""Automatically created by Django to configure the web application."""
from django.apps import AppConfig


class TlgBotConfig(AppConfig):
    """Tlg bot app configuration, automatically created Django class."""

    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tlg_bot'
