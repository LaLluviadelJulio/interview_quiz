"""Telegram bot main files."""

import logging
import os
from random import choice, randint

from asgiref.sync import sync_to_async

import django
from django.db.models import Q

from telegram import (
    InlineKeyboardButton, InlineKeyboardMarkup,
    ReplyKeyboardMarkup, Update,
)
from telegram.constants import ParseMode
from telegram.ext import (
    Application, CallbackQueryHandler,
    CommandHandler, ConversationHandler,
    MessageHandler, filters
)

# load django app and db interaction
import sys  # noqa I100
sys.path.append('/home/drf/interview_quiz/')
sys.path.append('/home/drf')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'interview_quiz.settings')
django.setup()

logger = logging.getLogger(__name__)

# import from django app only when app has already been loaded
from interview_quiz.settings import BOT_TOKEN, LEVEl_MAP  # noqa I100
from questions.models import Question, QuestionCategory  # noqa I100

from tlg_bot.models import TlgUserSettings  # noqa I100


# Synchronous functions

def get_start_keyboard():
    """Return the start keyboard."""
    return ReplyKeyboardMarkup(
         [['Выбор настроек'],
          ['Получить вопрос', 'Случайный вопрос'],
          ['Помощь']],
         resize_keyboard=True,
    )


def get_user_settings(update):
    """
    Get telegram user question settings from db.

    Args:
        update(Update): object to display the user interface;

    Returns:
        tuple(TlgUserSettings, tuple(QuestionCategory, str)):
            user settings, category and level of question
    """
    user_id = update.effective_user.id
    user_settings = TlgUserSettings.objects.get_or_create(
        tlg_chat_id=user_id)[0]
    return user_settings, user_settings.get_settings


# Synchronous functions to asynchronous (Django objects are involved)

@sync_to_async
def async_get_user_settings(update):
    """Get telegram user question settings from db asynchronously."""
    return get_user_settings(update)


@sync_to_async
def get_category_buttons(max_buttons_in_row=3):
    """
    Get category buttons for the reply keyboard.

    Args:
        max_buttons_in_row (int): quantity of the buttons in row

    Returns:
        dict, list
    """
    categories = QuestionCategory.objects.filter(available=True)
    cat_dict = {}
    for cat in categories:
        cat_dict.setdefault(cat.name, cat.id)
    cat_names, row_list = [], []

    for num, category in enumerate(categories):
        row_list.append(category.name)
        if len(row_list) == max_buttons_in_row:
            cat_names.append(row_list)
            row_list = []
        else:
            if num == len(categories) - 1:
                cat_names.append(row_list)
    return cat_dict, cat_names


@sync_to_async
def save_tlg_user_settings(update, context):
    """
    Save telegram user question settings to the db.

    Args:
        update (Update): object to display the user interface;
        context (ContextTypes.DEFAULT_TYPE): context storage;

    Returns:
        None:
    """
    cat_dict = context.user_data.get('cat_dict')
    if cat_dict and isinstance(cat_dict, dict):
        user_settings, _ = get_user_settings(update)
        category = context.user_data.get('category')
        level = LEVEl_MAP.get(context.user_data.get('level'))
        category_id = cat_dict.get(category)
        if category_id and level:
            category = QuestionCategory.objects.get(id=category_id)
            if category:
                user_settings.category = category
                user_settings.difficulty_level = level
                user_settings.save()
            else:
                logger.warning(
                    'Selected category %s does not exist. '
                    'Settings were not saved.',
                    category
                )
        else:
            logger.warning(
                'Selected category with id %s or level '
                '%s does not exist. Settings were not saved.',
                category_id, level
            )
    else:
        logger.warning(
            'Selected category does not exist. Settings were not saved.'
        )


@sync_to_async
def question_from_db(category, difficulty_level):
    """
    Get a question from the db.

    Args:
        category (QuestionCategory): selected question category;
        difficulty_level (str): selected question level;

    Returns:
        Question or None:
            random question with selected category and level if exist
    """
    question_set = Question.objects.filter(
        Q(subject=category),
        Q(difficulty_level=difficulty_level),
        Q(available=True)
    )
    if question_set:
        return question_set[randint(0, question_set.count() - 1)]
    return None


@sync_to_async
def get_random_setting():
    """
    Get a random category and level.

    Returns:
        tuple(QuestionCategory, str):
            random category and level of questions
    """
    categories = QuestionCategory.objects.all()
    random_category = categories[randint(0, categories.count() - 1)]
    _, random_level = choice(list(LEVEl_MAP.items()))
    return random_category, random_level


@sync_to_async
def save_last_question(user_settings, question):
    """
    Save current question to user telegram settings in the db.

    Args:
        user_settings (TlgUserSettings): current user settings;
        question (Question): current question;

    Returns:
        None:
    """
    user_settings.last_question = question
    user_settings.save()


# Asynchronous functions (Telegram bot actions)

async def start(update, context):
    """
    Send start message and keyboard.

    Args:
        update (Update): object to display the user interface;
        context (ContextTypes.DEFAULT_TYPE): context storage;

    Returns:
        None:
    """
    await update.message.reply_text(
        'Выберите настройки вопроса:',
        reply_markup=get_start_keyboard()
    )


async def parse_question(question, context, right_answer_num='0'):
    """
    Prepare the inline keyboard for the question.

    Args:
        question (Question): selected question;
        context (ContextTypes.DEFAULT_TYPE): context storage;
        right_answer_num (int): number of the right answer option;

    Returns:
        tuple (str, InlineKeyboardMarkup):
            text of the question and the keyboard with answer options.
    """
    ans_1, ans_2, ans_3, ans_4, right_answer = (
        question.answer_01, question.answer_02,
        question.answer_03, question.answer_04,
        question.right_answer
    )
    for num, i in enumerate((ans_1, ans_2, ans_3, ans_4)):
        if i == right_answer:
            right_answer_num = str(num + 1)
    keyboard = [
        [InlineKeyboardButton('1', callback_data='1')],
        [InlineKeyboardButton('2', callback_data='2')],
        [InlineKeyboardButton('3', callback_data='3')],
        [InlineKeyboardButton('4', callback_data='4')],
    ]
    context.user_data['right_answer'] = right_answer
    context.user_data['right_answer_num'] = right_answer_num
    reply_markup = InlineKeyboardMarkup(keyboard)
    text = f"""
    {question.question}

    1. {ans_1}
    2. {ans_2}
    3. {ans_3}
    4. {ans_4}
    """
    return text, reply_markup


async def get_random_question(update, context, attempts=5):
    """
    Get a question with random settings.

    Args:
        update (Update): object to display the user interface;
        context (ContextTypes.DEFAULT_TYPE): context storage;
        attempts (int): number of attempts at random settings,
                       if there are no issues with such settings;

    Returns:
        None:
    """
    user_settings, _ = await async_get_user_settings(update)
    question = None
    while not question:
        if attempts == 0:
            break
        random_category, random_level = await get_random_setting()
        question = await question_from_db(random_category, random_level)
        attempts -= 1
    if question:
        await save_last_question(user_settings, question)
        text, reply_markup = await parse_question(question, context)
        await update.message.reply_text(
            text=text, parse_mode=ParseMode.HTML,
            reply_markup=reply_markup
        )
        return
    await update.message.reply_text(
        'Извините, возникла проблема с получением случайного вопроса'
    )


async def get_question(update, context):
    """
    Get a question with selected settings.

    Args:
        update (Update): object to display the user interface;
        context (ContextTypes.DEFAULT_TYPE): context storage;

    Returns:
        None:
    """
    user_settings, category_and_level = await async_get_user_settings(update)
    question = await question_from_db(*category_and_level)
    if question:
        await save_last_question(user_settings, question)
        text, reply_markup = await parse_question(question, context)
        await update.message.reply_text(
            text=text, parse_mode=ParseMode.HTML,
            reply_markup=reply_markup
        )
        return
    await update.message.reply_text(
        'Вопросов с указанными настройками пока нет'
    )


async def check_answer(update, context):
    """
    Parse the CallbackQuery and check the answer.

    Args:
        update (Update): object to display the user interface;
        context (ContextTypes.DEFAULT_TYPE): context storage;

    Returns:
        None:
    """
    query = update.callback_query
    await query.answer()
    right_answer = context.user_data.get('right_answer')
    right_answer_num = context.user_data.get('right_answer_num')
    text = f"""
    Вопрос: {query.message.text}

    Правильный ответ: №{right_answer_num} - {right_answer}
    Ваш ответ: №{query.data}
    """
    if right_answer_num == query.data:
        await query.edit_message_text(
            text='Верно!\n' + text, parse_mode=ParseMode.HTML
        )
        return
    await query.edit_message_text(text=text, parse_mode=ParseMode.HTML)


async def set_category(update, context):
    """
    Set category of questions.

    Args:
        update (Update): object to display the user interface;
        context (ContextTypes.DEFAULT_TYPE): context storage;

    Returns:
        str: new key of the states.
    """
    cat_dict, names = await get_category_buttons()
    context.user_data['cat_dict'] = cat_dict

    keyboard = ReplyKeyboardMarkup(names, resize_keyboard=True,)
    await update.message.reply_text(
        'Выберите категорию: ',
        reply_markup=keyboard
    )
    return 'category'


async def set_level(update, context):
    """
    Set level of questions.

    Args:
        update (Update): object to display the user interface;
        context (ContextTypes.DEFAULT_TYPE): context storage;

    Returns:
        str: new key of the states.
    """
    context.user_data['category'] = update.message.text
    keyboard = ReplyKeyboardMarkup(
        [['Легкий', 'Средний', 'Сложный'], ],
        resize_keyboard=True,
    )
    await update.message.reply_text(
        'Выберите сложность:',
        reply_markup=keyboard
    )
    return 'level'


async def confirm_settings(update, context):
    """
    Confirm question settings.

    Args:
        update (Update): object to display the user interface;
        context (ContextTypes.DEFAULT_TYPE): context storage;

    Returns:
        str: new key of the states.
    """
    keyboard = ReplyKeyboardMarkup(
        [['Да', 'Нет'], ],
        resize_keyboard=True,
    )
    context.user_data['level'] = update.message.text
    text = f"""
    Выбраны настройки:
    <b>Категория:</b> {context.user_data.get('category')}
    <b>Уровень:</b> {context.user_data.get('level')}
    Подтвердить?
    """

    await update.message.reply_text(
        text, parse_mode=ParseMode.HTML,
        reply_markup=keyboard)
    return 'is_ended'


async def exit_settings(update, context):
    """
    Close the conversation for selecting the question settings.

    Args:
        update (Update): object to display the user interface;
        context (ContextTypes.DEFAULT_TYPE): context storage;

    Returns:
        int: status of the end of the conversation.
    """
    await save_tlg_user_settings(update, context)
    text = f"""
    Выбраны настройки:
    <b>Категория:</b> {context.user_data.get('category')}
    <b>Уровень:</b> {context.user_data.get('level')}
    """

    await update.message.reply_text(
        text, parse_mode=ParseMode.HTML,
        reply_markup=get_start_keyboard()
    )
    return ConversationHandler.END


async def not_known(update, context):
    """
    Send a response when receiving a message in an unknown format.

    Args:
        update (Update): object to display the user interface;
        context (ContextTypes.DEFAULT_TYPE): context storage;

    Returns:
        None:
    """
    await update.message.reply_text(
        'Получен неизвестный формат ответа.\nВыберите один из предложенных'
    )


async def help_command(update, context):
    """
    Display info on how to use the bot.

    Args:
        update (Update): object to display the user interface;
        context (ContextTypes.DEFAULT_TYPE): context storage;

    Returns:
        None:
    """
    text = """
    Функции бота: выдача вопросов для самопроверки теории.

    Кнопки:
    • <b>Выбор настроек:</b> задать категорию и уровень сложности вопроса
    • <b>Получить вопрос:</b> получить вопрос заданной категории и уровня
    • <b>Случайный вопрос:</b> получить случайно выпадающий вопрос
    """

    await update.message.reply_text(
        text, parse_mode=ParseMode.HTML,
    )


def run_bot():
    """
    Run the bot.

    Returns:
        None:
    """
    application = Application.builder().token(BOT_TOKEN).build()
    application.add_handler(CommandHandler('start', start))
    application.add_handler(
        MessageHandler(filters.Regex('Помощь'), help_command)
    )
    application.add_handler(
        MessageHandler(filters.Regex('Получить вопрос'), get_question)
    )
    application.add_handler(
        MessageHandler(filters.Regex('Случайный вопрос'), get_random_question)
    )

    application.add_handler(CallbackQueryHandler(check_answer))

    application.add_handler(ConversationHandler(
        entry_points=[MessageHandler(
            filters.Regex('Выбор настроек'),
            set_category),
        ],
        states={
            'category': [MessageHandler(filters.TEXT, set_level)],
            'level': [MessageHandler(
                (filters.Regex('Легкий') | filters.Regex('Средний')
                 | filters.Regex('Сложный')),
                confirm_settings)],
            'is_ended': [
                MessageHandler(filters.Regex('Да'), exit_settings),
                MessageHandler(filters.Regex('Нет'), set_category)]
        },
        fallbacks=[
            MessageHandler(
                (filters.VIDEO |
                 filters.PHOTO | filters.ATTACHMENT |
                 filters.AUDIO | filters.ANIMATION
                 ),
                not_known
            )
        ]
    ))

    application.run_polling(allowed_updates=Update.ALL_TYPES)


if __name__ == '__main__':
    run_bot()
