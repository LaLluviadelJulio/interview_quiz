"""
Stores the telegram user question settings model.

It is necessary to interact with the telegram bot to get questions of
the selected category and level.
"""

from django.db import models

from interview_quiz.settings import AVERAGE, NEWBIE, SMARTYPANTS

from questions.models import Question, QuestionCategory


class TlgUserSettings(models.Model):
    """Model of the telegram user question settings."""

    DIFFICULTY_LEVEL_CHOICES = (
        (NEWBIE, 'новичок'),
        (AVERAGE, 'середнячок'),
        (SMARTYPANTS, 'умник'),
    )
    category = models.ForeignKey(
        QuestionCategory, on_delete=models.CASCADE,
        default='1',
    )
    tlg_chat_id = models.PositiveIntegerField()

    difficulty_level = models.CharField(
        choices=DIFFICULTY_LEVEL_CHOICES,
        verbose_name='уровень', max_length=2,
        default=NEWBIE, db_index=True
    )

    last_question = models.ForeignKey(
        Question, on_delete=models.CASCADE,
        verbose_name='последний вопрос',
        blank=True, null=True
    )

    def __str__(self):
        """Forms a printable representation of the object."""
        return (
            f'{self.tlg_chat_id} lvl: {self.difficulty_level} '
            f'category: {self.category}'
        )

    @property
    def get_settings(self):
        """Return the saved category and level."""
        return self.category, self.difficulty_level
