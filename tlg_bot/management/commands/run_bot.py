"""Contains custom command for easy run the Telegram bot by manage.py."""

import logging

from django.core.management import BaseCommand

from tlg_bot.bot_main import run_bot

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    """Django command class."""

    name = 'run_bot'
    help = (  # noqa A003
        'Run Telegram bot.'
    )

    def handle(self, *args, **kwargs):
        """Start the Telegram bot command."""
        try:
            self.stdout.write('Run telegram bot')
            run_bot()
            logger.info('Command %s, result: bot running', self.name,)
        except Exception as err:
            self.stdout.write(err)
            logger.error('Command %s, err: %s', self.name, err)
