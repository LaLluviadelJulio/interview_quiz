"""Contains custom command for easy set and remove webhook by manage.py."""

import logging

from django.core.management import BaseCommand

from interview_quiz.settings import BOT_TOKEN, WEBHOOK_TOKEN

import requests


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    """Django command class."""

    name = 'set_webhook'
    help = (  # noqa A003
        'Set Telegram webhooks.'
        'To remove webhook leave empty url option.'
    )

    def add_arguments(self, parser):
        """
        Add optional url argument.

        Leave it empty to remove webhook.
        """
        parser.add_argument(
            '-u', '--url',
            type=str,
            help='The url to make a webhook to.'
        )

    def handle(self, *args, **kwargs):
        """Set and remove Telegram webhook command."""
        try:
            webhook_url = kwargs.get('url') or ''
            tlg_url = (
                    f'https://api.telegram.org/bot{BOT_TOKEN}'
                    f'/setWebhook?url={webhook_url}'
            )
            if webhook_url:
                tlg_url = f'{tlg_url}/{WEBHOOK_TOKEN}/'

            result = requests.get(tlg_url)
            description = result.json().get('description')
            self.stdout.write(description)
            self.stdout.write(tlg_url)
            logger.info('Command %s, result: %s', self.name, description)
        except Exception as err:
            self.stdout.write(err)
            logger.error('Command %s, err: %s', self.name, err)
